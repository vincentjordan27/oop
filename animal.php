<?php 
    class Animal {
        private $name;
        private $legs = 2;
        private $cold_blooded = "false";
        public function __construct($name){
            $this->name = $name;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_cold_blooded(){
            return $this->cold_blooded;
        }

        public function get_legs(){
            return $this->legs;
        }

    }
?>