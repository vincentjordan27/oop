<?php 
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo $sheep->get_name() . "<br>";
echo $sheep->get_legs() . "<br>";
echo $sheep->get_cold_blooded() . "<br><br>";

$sungokong = new Ape("kera sakti");
echo $sungokong->yell() . "<br><br>";


$kodok  = new Frog("buduk");
echo $kodok->jump() . "<br><br>";


?>