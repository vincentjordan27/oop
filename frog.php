<?php 

    class Frog extends Animal{
        private $legs = 4;

        public function get_legs(){
            return $this->legs;
        }

        public function jump(){
            return "hop hop";
        }
    }


?>